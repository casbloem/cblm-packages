window.Vue = require("vue");

const vueLuxon = require("./vue-luxon.js");
const { DateTime } = require("luxon");

Vue.use(vueLuxon, {
  serverFormat: "ISO",
  localeLang: 'en',
});

const vueMny = require("vue-mny");

Vue.use(vueMny, {
  locale: "ru-RU",
  style: "currency",
  currency: "RUB"
});

Vue.component("vue-luxon-example", require("../vue-luxon.vue"));

const app = new Vue({
  el: "#app",
  data() {
    return {
      input1: 158,
      symbolPosition: 0,
      testlocale: "nl-NL",
      teststyle: "currency",
      testcurrency: "EUR",
      testcurrencyDisplay: "code"
    };
  }
});
