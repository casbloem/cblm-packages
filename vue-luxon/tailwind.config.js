module.exports = {
  purge: {
    enabled: true,
    mode: 'layers',
    content: [
      './src/*.vue',
      './index.html'
    ]
  },
  theme: {
    fontFamily: {
      sans: "'Inter', sans-serif"
    },
    extend: {},
  },
  variants: {
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
  },
  plugins: [],
  future: {
    removeDeprecatedGapUtilities: true,
  },
}