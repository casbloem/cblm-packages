//
// To run this:  `rollup -c`
//


import vue from 'rollup-plugin-vue';
import postcss from 'rollup-plugin-postcss';
import autoprefixer from 'autoprefixer';
import clean from 'postcss-clean';
import babel from '@rollup/plugin-babel';
import commonjs from "@rollup/plugin-commonjs";
import nodeResolve from "@rollup/plugin-node-resolve";
import {
    terser
} from "rollup-plugin-terser";
//import cleaner from 'rollup-plugin-cleaner';
import replace from "@rollup/plugin-replace";
import cleanup from 'rollup-plugin-cleanup';
import banner from 'rollup-plugin-banner';
import tailwind from "tailwindcss";


// Settings verwerken


const ENV = 'development'; // production / development

const PLUGINS = {
    dev: [
        replace({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),
        vue({
            css: false
        }),
        postcss({
            extract: true,
            plugins: [tailwind()]
        }),
        nodeResolve({
            mainFields: ['main', 'browser']
        }),
        commonjs({
            sourceMap: false
        }),

    ],
    prod: [
        replace({
            'process.env.NODE_ENV': JSON.stringify(ENV)
        }),
        vue({
            css: false
        }),
        postcss({
            extract: true,
            plugins: [tailwind(), autoprefixer(), clean()]
        }),
        nodeResolve({
            mainFields: ['main', 'browser']
        }),
        commonjs({
            sourceMap: false
        }),
        babel({
            babelHelpers: 'bundled',
            exclude: [/\/core-js\//]
        }),
        terser(),
        cleanup({
            comments: 'none'
        }),
        banner('application by Cas Bloem')
    ]
}

// Export Rollup config object
export default (commandLineArgs) => {
    return {
        output: {
            format: 'iife',
            strict: false


        },
        plugins: commandLineArgs.environment == 'production' ? PLUGINS.prod : PLUGINS.dev,
    }
};