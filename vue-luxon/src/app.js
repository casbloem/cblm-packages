import Vue from "vue";

Vue.config.productionTip = false;

import App from "./App.vue";

import vueLuxon from 'F:/repositories/vue-luxon/dist/vue-luxon.min.js';
Vue.use(vueLuxon);



new Vue({
    render: (h) => h(App),
}).$mount("#vue");